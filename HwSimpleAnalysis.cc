// -*- C++ -*-
//
// HwSimpleAnalysis.cc is a part of Herwig - A multi-purpose Monte Carlo event generator
// Copyright (C) 2002-2016 The Herwig Collaboration
//
// Herwig++ is licenced under version 2 of the GPL, see COPYING for details.
// Please respect the MCnet academic guidelines, see GUIDELINES for details.
//
//
// This is the implementation of the non-inlined, non-templated member
// functions of the HwSimpleAnalysis class.
//

#include <cmath>
#include <fstream>
#include <iostream>
#include <sys/stat.h>
#include <sys/types.h>

#include "HwSimpleAnalysis.h"

#include "Herwig/Utilities/Histogram.h"
#include "ThePEG/Repository/EventGenerator.h"
#include "ThePEG/EventRecord/Particle.h"
#include "ThePEG/Vectors/ThreeVector.h"
#include "ThePEG/EventRecord/Event.h"
#include "ThePEG/PDT/EnumParticles.h"
#include "ThePEG/Interface/Switch.h"
#include "ThePEG/Interface/Reference.h"
#include "ThePEG/Repository/UseRandom.h"
#include "ThePEG/Interface/Parameter.h"
#include "ThePEG/Interface/ClassDocumentation.h"
#include "ThePEG/Persistency/PersistentOStream.h"
#include "ThePEG/Persistency/PersistentIStream.h"

//Fastjet headers
#include "fastjet/PseudoJet.hh"
#include "fastjet/ClusterSequence.hh"
#include "fastjet/tools/MassDropTagger.hh"
#include "fastjet/tools/Filter.hh"
#include "fastjet/ClusterSequenceArea.hh"
#include <fastjet/tools/JHTopTagger.hh>
#include <fastjet/Selector.hh>

using namespace std;
using namespace fastjet;
using namespace Herwig;

HwSimpleAnalysis::HwSimpleAnalysis()
  :jetAlgorithm_(-1), cut_eta_(5.0), cut_pt_part_(0.1), cut_pt_jet_(20.0), cut_eta_jet_(5.0),  R_(0.4) {}

IBPtr HwSimpleAnalysis::clone() const {
  return new_ptr(*this);
}

IBPtr HwSimpleAnalysis::fullclone() const {
  return new_ptr(*this);
}

void HwSimpleAnalysis::persistentOutput(PersistentOStream & os) const {
  os  << jetAlgorithm_ << cut_eta_ << cut_pt_part_ << cut_pt_jet_ << cut_eta_jet_ << R_;

}

void HwSimpleAnalysis::persistentInput(PersistentIStream & is, int) {
  is >> jetAlgorithm_ >> cut_eta_ >> cut_pt_part_ >> cut_pt_jet_ >> cut_eta_jet_ >> R_;
}

bool pTsortFunction(PPtr i,PPtr j) {
  return (i->momentum().perp2()>j->momentum().perp2());
}
bool ETsortFunction(pair<Energy, Lorentz5Momentum> i,pair<Energy, Lorentz5Momentum> j) {
  return (i.first>j.first);
}

/***********************************************************************
 * MAIN ANALYSIS FUNCTION
************************************************************************/
void HwSimpleAnalysis::analyze(tEventPtr event, long, int, int) {
  
  /*
   * COUNT THE TOTAL NUMBER OF EVENTS
   */
  numevents_++;

  /* 
   * GET THE EVENT WEIGHT
   */ 
  evweight = event->weight();
  
  /*
   * GET THE FINAL-STATE PARTICLES
   */
  event->getFinalState( particlesToCluster_ );
  numparticles = particlesToCluster_.size();  //get the total number of particles

  /*
   * GET THE VECTOR OF INTERMEDIATE PARTICLES
   * IN HARD PROCESS AND LOOP OVER THEM
   * (FOR FUTURE USE)
   */ 
  ParticleVector intermediates(event->primarySubProcess()->outgoing());
  //search primary collision
  StepVector::const_iterator sit =event->primaryCollision()->steps().begin();
  StepVector::const_iterator send=event->primaryCollision()->steps().end(); 
  ParticleSet partW=(**sit).all();
  ParticleSet::const_iterator iterrW=partW.begin();
  ParticleSet::const_iterator enddW =partW.end();
  Lorentz5Momentum pcc(0.*GeV,0.*GeV,0.*GeV,0.*GeV,0.*GeV);

  for(unsigned int ww = 0; ww < intermediates.size(); ww++) { 
    pcc = intermediates[ww]->momentum(); 
    //  cout << "final momentum: x,y,z,e = " << pcc.x()/GeV << " " << pcc.y()/GeV << " " << pcc.z()/GeV << " " << pcc.e()/GeV << endl;
  }
  
  /* 
   * LOOP OVER FINAL-STATE PARTICLES AND PUT THEM IN 
   * THE OBJECTS ARRAY
   */
  int Id = 0, absId = 0;
  Lorentz5Momentum pz;
  int Charge = 0;
  for(unsigned int ii = 0; ii < particlesToCluster_.size(); ii++) {

    absId=fabs(particlesToCluster_[ii]->id()); //get abs(id) of particle
    Id = particlesToCluster_[ii]->id(); //id of particle
    Charge = particlesToCluster_[ii]->dataPtr()->iCharge(); //charge of particle
    pz = particlesToCluster_[ii]->momentum(); //put the momentum in a Lorentz5Momentum
    objects[0][ii] = pz.e()/GeV; //energy
    objects[1][ii] = pz.x()/GeV; //px
    objects[2][ii] = pz.y()/GeV; //py 
    objects[3][ii] = pz.z()/GeV; //pz
    objects[4][ii] = Id; //Id
    objects[5][ii] = Charge/3.0; //Charge +-1, 0
    objects[6][ii] = 0;//extra identifiers (for future use)
    objects[7][ii] = 0;
  }
    
  
  /* FIND JETS
   */


  /* global cut on all particles and min cut on jets */
  double cut_eta(cut_eta_); //global pseudorapidity cut
  double cut_pt_part(cut_pt_part_); //global pt cut for particles [GeV]

  /* jet cuts */
  double cut_pt_jet(cut_pt_jet_); //pt cut for jets
  double cut_eta_jet(cut_eta_jet_); //pseudo-rapidity cut for jets

  /*jet algorithm */
  double R(R_);
  
 
 

  //clear particle vectors for next use
  particlesToCluster_.clear();

}

/***********************************************************************
 This is where the main analysis function ends
************************************************************************/

ClassDescription<HwSimpleAnalysis> HwSimpleAnalysis::initHwSimpleAnalysis;
// Definition of the static class description member.

//The Following provides INTERFACES to the various settings of the analysis
void HwSimpleAnalysis::Init() {

  static ClassDocumentation<HwSimpleAnalysis> documentation
    ("The HwSimpleAnalysis class has been created for the simple object reconstruction");
  
  static Switch<HwSimpleAnalysis,int> interfaceJetAlgorithm
    ("JetAlgorithm",
     "Determines the jet algorithm for finding jets in parton-jet ",
     &HwSimpleAnalysis::jetAlgorithm_, -1, false, false);
  static SwitchOption AntiKt
    (interfaceJetAlgorithm,
     "AntiKt",
     "The anti-kt jet algorithm.",
     -1);
  static SwitchOption CambridgeAachen
    (interfaceJetAlgorithm,
     "CambridgeAachen",
     "The Cambridge-Aachen jet algorithm.",
     0);
  static SwitchOption Kt
    (interfaceJetAlgorithm,
     "Kt",
     "The Kt jet algorithm.",
     1);
  
    static Parameter<HwSimpleAnalysis, double> interfacePTCutParticles
    ("PTCutParticles",
     "Minimum particle pT to be considered.",
     &HwSimpleAnalysis::cut_pt_part_, 0.1, 0.0, 100000.0,
     false, false, Interface::limited);

    static Parameter<HwSimpleAnalysis, double> interfaceEtaCutParticles
    ("EtaCutParticles",
     "Minimum particle pT to be considered.",
     &HwSimpleAnalysis::cut_eta_, 5.0, 0.0, 100000.0,
     false, false, Interface::limited);

    static Parameter<HwSimpleAnalysis, double> interfacePTCutJets
    ("PTCutJets",
     "Minimum jet pT to be considered.",
     &HwSimpleAnalysis::cut_pt_jet_, 20.0, 0.0, 100000.0,
     false, false, Interface::limited);

    static Parameter<HwSimpleAnalysis, double> interfaceEtaCutJets
    ("EtaCutJets",
     "Minimum jet pT to be considered.",
     &HwSimpleAnalysis::cut_eta_jet_, 5.0, 0.0, 100000.0,
     false, false, Interface::limited);

    static Parameter<HwSimpleAnalysis, double> interfaceRParameter
    ("RParameter",
     "Jet algorithm R-parameter",
     &HwSimpleAnalysis::R_, 0.4, 0.0, 100000.0,
     false, false, Interface::limited);
}


/***********************************************************************
 This function is executed at the START of the analysis
************************************************************************/
void HwSimpleAnalysis::doinitrun() {
  //initialize Analysis handler
  AnalysisHandler::doinitrun();
 
  //output to screen 
  std::cout << "HwSimpleAnalysis Loaded." << endl;
  
  //reset number counters
  numevents_=0.;
}

/***********************************************************************
 This function is executed at the END of the analysis
************************************************************************/
void HwSimpleAnalysis::dofinish() {
  //finish Analysis handler
  AnalysisHandler::dofinish();
  std::cout << "HwSimpleAnalysis done." << endl;
}



